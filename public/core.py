from js import document, window
from collections import Counter

from pyodide.http import pyfetch
from pyodide.ffi import create_proxy
import pyscript
import asyncio
from dataclasses import dataclass
from functools import partial


import js
query_string = js.window.location.search

try:
    options = {}
    query_string=query_string[1:]
    queries = query_string.split('&')
    for query in queries:
        name, value = query.split('=')
        print(name, value)
        options[name] = value
except ValueError:
    options = {}
    
def goto_level(n, _junk=None):
    window.location.href = './?level={}'.format(n)
    

document.getElementById("prev").addEventListener("click", create_proxy(partial(goto_level, int(options.get("level",1)) - 1)))
document.getElementById("next").addEventListener("click", create_proxy(partial(goto_level, int(options.get("level",-1)) + 1)))

def build_table(listoflists):
    return "\n".join(
            "<tr>{}</tr>".format(
                " ".join(
                    "<td>{}</td>".format(l)
                    for l in lst
                )
            )
            for lst in listoflists
        )
    
def build_anno_table(listoflists):
    return "\n".join(
            "<tr id=\"{}\">{}</tr>".format(
                anno,
                " ".join(
                    "<td>{}</td>".format(l)
                    for l in lst
                )
            )
            for anno, lst in listoflists
        )
    
table_wrapper = "<table>{}</table>".format
    
def display_cost(ctr):
    def helper():
        for k,v in ctr.items():
            if v < 8:
                yield f"<img src=\"images/{k}.png\">" * v
            else:
                yield f"{v}x <img src=\"images/{k}.png\">"
    return ''.join(helper())
    
    
def display_cost_twice(ctr1, ctr2):
    def helper():
        for k,v1 in ctr1.items():
            v2 = ctr2[k]
            Δv = v1 - v2
            if Δv > 0:
                yield f"<img src=\"images/{k}.png\" class=\"av\">" * v2 + \
                    f"<img src=\"images/{k}.png\" class=\"missing\">" * Δv
            else:
                yield f"<img src=\"images/{k}.png\" class=\"av\">" * v1
    res = ''.join(helper())
    if res:
        return res
    else:
        return "free"

@dataclass(frozen=True)
class Flower:
    name: str
    provide: Counter
    cost: Counter
    flower_template = """
    <div class="flower">
        <span id="flower_{index}_root">
            <div class="plant"><img src="images/flower.png" id="flower_{index}_image">
            <div class="flower1"><img src="images/waterlily.png" id="flower_{index}_image_0"></div>
            <div class="flower2"><img src="images/snowdrop.png" id="flower_{index}_image_1"></div>
            <div class="flower3"><img src="images/bellflower.png" id="flower_{index}_image_2"></div>
            <div class="flower4"><img src="images/snowdrop.png" id="flower_{index}_image_3"></div>
            <div class="flower5"><img src="images/fireblossom.png" id="flower_{index}_image_4"></div>
            </div>
        </span>
    </div>
    """
    @staticmethod
    def initialise_flowers():
        flowerpots = [Flower.flower_template.format(index=i) for i in range(5)]
        killbuttons = [f"<button id=\"button_kill_{i}\" class=\"kill\"> remove </button>" for i in range(5)]
        undo_button = ["""<button id="button_undo">undo</button>"""]
        
        document.getElementById("inventory").innerHTML = table_wrapper(build_table([flowerpots, killbuttons + undo_button]))
        
    @property
    def flowertype(self):
        return next(iter(self.provide.keys()))
    
    @property
    def level(self):
        return next(iter(self.provide.values()))

    @property
    def row_id(self):
        return f"store_row_{self.name}"
    
    @property
    def button_id(self):
        return f"store_button_{self.name}"
    
    @property
    def cost_id(self):
        return f"store_cost_{self.name}"
        
    def display_flower(self, location):
        for i in range(5):
            div = document.getElementById(f"flower_{location}_image_{i}")
            div.src = f"images/{self.flowertype}.png"
            div.hidden = i >= self.level
        div = document.getElementById(f"flower_{location}_image")
        div.src = f"images/flower.png"
            
    @staticmethod
    def remove_flower(location):
        for i in range(5):
            div = document.getElementById(f"flower_{location}_image_{i}")
            div.hidden = True
        div = document.getElementById(f"flower_{location}_image")
        div.src = f"images/flowerpot.png"
        
        
    @staticmethod    
    def display_store_table_generator( flowers):
        for flower in flowers:
            yield flower.row_id, [
                display_cost(flower.provide),
                f"<span id=\"{flower.cost_id}\">{display_cost(flower.cost)}</span>",
                f"<button id=\"{flower.button_id}\">grow</button>"
            ]
            
    @staticmethod
    def display_store(flowers):
        print("update store")
        table = build_anno_table(list(Flower.display_store_table_generator(flowers)))
        print("finished table")
        document.getElementById(f"store").innerHTML = f"""
        <table class="store">
        <tr><th>Yield</th><th>Requirement</th><th></th></tr>
        {table}
        </table>
        """.strip()
        print("updated store")
        
    def __eq__(self, other):
        return other is not None and self.name == other.name
    
    @staticmethod
    def find( name):
        return next((x for x in Flower.all_ if x.name == name))
        
        
        
class Player:
    def __init__(self):
        self.history = []
        self.flowers = [None, None, None, None, None]
        self.store_display_mode = "all"
        
    def delete_flower(self, index, event_object=None):
        self.history.append(self.flowers.copy())
        self.flowers[index] = None
        self.update_interface()
        
    def delete_flower_eq(self, flower):
        self.delete_flower(self.flowers.index(flower))
        
        
    
        
    def build_flower(self, flower):
        try:
            index = self.flowers.index(None)
            self.history.append(self.flowers.copy())
        except IndexError:
            raise AssertionError("cannot build a flower if there is no free spot")
        self.flowers[index] = flower
        
        if "starflower" in flower.name:
            document.getElementById("win").hidden = False
        
    def undo(self, event_object=None):
        self.flowers = self.history.pop()
        self.update_interface()
    
    def build_flower_if_allowed(self, flower, event_object=None):
        print("build", flower.name)
        if self.can_be_build(flower) and None in self.flowers:
            self.build_flower(flower)
        print("done build", flower.name)
        self.update_interface()
            
    def update_interface(self):
        self.display_flowers()
        self.update_store_display()
        self.update_cost_display()
        document.getElementById("resources").innerHTML = display_cost(self.resources())
        
    def can_be_build(self, flower):
        return not (flower.cost - self.resources())
        
    def resources(self):
        return sum((x.provide for x in self.flowers if x), start=Counter({}))
        
    def display_flowers(self):
        for i,x in enumerate(self.flowers):
            if x is None:
                Flower.remove_flower(i)
            else:
                x.display_flower(i)
                
    def update_store_display(self):
        for flower in Flower.all_:
            document.getElementById(flower.button_id).disabled = not self.can_be_build(flower)
            if self.store_display_mode == "all":
                document.getElementById(flower.row_id).hidden = False
            elif self.store_display_mode == "affort":
                document.getElementById(flower.row_id).hidden = not self.can_be_build(flower)
            elif self.store_display_mode == "recommended":
                document.getElementById(flower.row_id).hidden = False 
                
    def update_cost_display(self):
        for flower in Flower.all_:
            document.getElementById(flower.cost_id).innerHTML = display_cost_twice(flower.cost, self.resources())
            
                
    def annotate(self, flowers):
        for flower in flowers:
            print("annotated", flower.name)
            document.getElementById(flower.button_id).addEventListener("click", create_proxy(partial(self.build_flower_if_allowed, flower)))
            
        for i in range(5):
            document.getElementById(f"button_kill_{i}").addEventListener("click", create_proxy(partial(self.delete_flower, i)))
            
        document.getElementById(f"button_undo").addEventListener("click", create_proxy(self.undo))
            
        
    async def exec_solution(self):
        print("exec solution")
        await asyncio.sleep(0.5)
        for i in range(4):
            self.build_flower_if_allowed(Flower.all_[0])
            print("build init")
            await asyncio.sleep(0.2)
            print("build init 2")
        
        for x, y in self.solution:
            self.build_flower_if_allowed(Flower.find(y))
            print("build", y)
            await asyncio.sleep(0.2)
            self.delete_flower_eq(Flower.find(x))
            print("burn", x)
            await asyncio.sleep(0.2)
            
        self.build_flower_if_allowed(Flower.all_[-1])
        
        await asyncio.sleep(1)
        
        goto_level(int(options.get("level",0)) + 1)
        
        


async def load_level():
    print("loading level")
    flowers = []
    response = await pyfetch("levels.json", method= "GET")
    
    data = await response.json() 
    
    print("levels:", len(data))
    try:
        for k,v in data[int(options.get("level",0))]["setup"].items():
            flowers.append(Flower(k, Counter(v[0]), Counter(v[1])))
        return flowers, data[int(options.get("level",0))]["solution"]
    except IndexError:
        goto_level(0)
        
    
def log(newtext):
    document.getElementById(f"loading_us").innerHTML = newtext
    
async def main():
    if "level" in options:
        log("Loading levels from file")
        flowers, solution = await load_level()
        Flower.all_ = flowers
        log("Initialising flower graphics")
        Flower.initialise_flowers()
        log("Initialising spell list graphics")
        Flower.display_store(flowers)
        log("Initialising player")
        player = Player()
        log("Initialising player flowers")
        player.display_flowers()
        log("Linking buttons to events")
        player.annotate(flowers)
        log("Initialising solver")
        player.solution = solution
        log("Update interface")
        player.update_interface()
        log("Unhiding Gameboard")
        
        document.getElementById(f"gameboard").hidden = False
        
    document.getElementById(f"loading").hidden = True
    
    # await player.exec_solution()
    
    
    
    
pyscript.run_until_complete(main())

