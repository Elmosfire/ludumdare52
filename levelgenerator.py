from dataclasses import dataclass
from typing import Dict, Tuple, List, Any
from collections import Counter, deque
import json

flowernames = ["fireblossom", "waterlily", "bellflower", "snowdrop"]


@dataclass(frozen=True)
class Flower:
  flower: str
  level: int
  cost: Counter

  @property
  def provide(self):
    return Counter({self.flower: self.level})

  def is_downgrade_off(self, other):
    return self.flower == other.flower and self.level <= other.level

  def lexical(self):
    return (self.flower, self.level)

  def __le__(self, other):
    return self.lexical() < other.lexical()

  def can_affort(self, resources):
    return not (self.cost - resources)

  def name(self):
    return f"{self.flower}_{self.level}"



@dataclass(frozen=True)
class State:
  content: List[Flower]
  prev: Any
  change: Any

  @classmethod
  def build(self, flowers):
    return State(sorted(flowers, key=Flower.lexical), None, None)

  def sub(self, flowers, ch=None):
    return State(sorted(flowers, key=Flower.lexical), self, ch)

  def swap(self, location, newflower):
    newcontent = self.content.copy()
    newcontent[location] = newflower
    return self.sub(newcontent, (self.content[location], newflower))
  
  def provide(self):
    return sum((x.provide for x in self.content), start=Counter())

  def can_affort(self, flowerlist):
    resources = self.provide()
    for flower in flowerlist:
      if flower.can_affort(resources):
        yield flower

  def is_downgrade_off(self, other):
    return all(a.is_downgrade_off(b) for a,b in zip(self.content, other.content))

  def branch(self, flowerlist):
    for x in self.can_affort(flowerlist):
      for y in range(4):
        yield self.swap(y, x)

  def difficulty(self):
    if self.prev is None:
      return 0
    else:
      return self.prev.difficulty() + 1

  def full_history(self):
    if self.prev is not None:
      yield from self.prev.full_history()
    yield self


def find_all_states(flowerlist):
  hist = []
  Q = deque()

  def prune(state):
    return any(state.is_downgrade_off(x) for x in hist) or \
      any(state.is_downgrade_off(x) for x in Q)


  Q.append(State.build([flowerlist[0]] * 4))

  while Q:
    current = Q.popleft()

    if prune(current):
      continue

    hist[:] = [x for x in hist if not x.is_downgrade_off(current)]
    hist.append(current)

    for newstate in current.branch(flowerlist):
      if prune(newstate):
        continue
      Q.append(newstate)
  
  return hist


from functools import reduce

from operator import or_

from random import choice

class FlowerManager:
  def __init__(self):
    self.flowerlist = []
    self.add_new_card("waterlily", Counter())

  def reach(self):
    return reduce(or_, (x.provide for x in self.flowerlist), Counter({k:0 for k in flowernames}))

  def add_new_card(self, flowertype, costCounter):
    value = self.reach()[flowertype] + 1
    self.flowerlist.append(Flower(flowertype, value, costCounter.copy()))

  def get_all_end_situations(self):
    return find_all_states(self.flowerlist)

  def get_all_end_resources_annotated(self):
    return list(sorted(((x.difficulty(), x.provide(), x) for x in self.get_all_end_situations()), key=lambda x:-x[0]))



  def increase_difficulty(self, drop=0):
    reach = self.reach() + Counter({k:1 for k in flowernames})
    print(reach)
    lowest = min(reach.values())
    flower_nxt = choice([k for k,v in reach.items() if v == lowest])

    alldata = self.get_all_end_resources_annotated()
    diff, cost, _ = alldata[0]

    costs = [x[1] for x in alldata]

    repl = sum(not (cost - x) for x in costs)

    print("replicate", repl)

    while drop and sum(cost.values()) > 2:
      base = choice([k for k,v in cost.items() if v > 0])
      cost[base] -= 1
      newreplicate = sum(not (cost - x) for x in costs)
      if newreplicate > repl:
        print("invalid replicate", newreplicate)
        cost[base] += 1
      drop -= 1

    self.add_new_card(flower_nxt, cost)
    print("difficulty", diff)
    
  def finalise(self):
    diff, cost, final = self.get_all_end_resources_annotated()[0]

    self.add_new_card("starflower", cost)
    
    self.final = final
    print("difficulty final", diff)


def generate_level(layers, drop=0):
  fm = FlowerManager()
  for _ in range(layers):
    if choice([False, False, True]):
      fm.increase_difficulty(drop)
    else:
      fm.increase_difficulty(0)

  fm.finalise()


  rs = {}

  for x in fm.flowerlist:
    rs[x.name()] = (dict(x.provide), dict(x.cost))

  solution = [[y.name() for y in x.change] for x in fm.final.full_history() if x.change]

  return dict(setup=rs, solution=solution, layers=layers, difficulty=fm.final.difficulty())


generate_level(2, 2)


from tqdm.notebook import tqdm

all_ = []
for layers in tqdm(range(5, 20)):
  for drop in range(0, 3):
    loc = []
    for _ in range(3):
      loc.append(generate_level(layers, drop))
    all_.append(max(loc, key=lambda x:x["difficulty"]))

all_.sort(key=lambda x:  (x["layers"], x["difficulty"]))

with open("levels.json", "w") as file:
  json.dump(all_, file, indent=2)
  









